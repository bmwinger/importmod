import requests
import json
import hashlib
import os
from collections import namedtuple
from portmod.repo import Atom
from portmod.repo.util import get_max_version, get_hash
from portmod.repo.download import get_filename
from .atom import parse_name, parse_version
from .config import NEXUS_KEY

NexusData = namedtuple(
    "NexusData", ["atom", "name", "desc", "files", "homepage", "author", "nudity"]
)


class APILimitExceeded(Exception):
    pass


def get_nexus_info(game, modid):
    info_url = f"https://api.nexusmods.com/v1/games/{game}/mods/{modid}/"
    files_url = f"https://api.nexusmods.com/v1/games/{game}/mods/{modid}/files/"

    headers = {"APIKEY": NEXUS_KEY, "content-type": "application/json"}

    rinfo = requests.get(info_url, headers=headers)
    if rinfo.headers["X-RL-Daily-Remaining"] == 0:
        raise APILimitExceeded()

    rfiles = requests.get(files_url, headers=headers)

    if rinfo.status_code == rfiles.status_code == requests.codes.ok:
        info = json.loads(rinfo.text)
        files = json.loads(rfiles.text)
    else:
        rinfo.raise_for_status()
        rfiles.raise_for_status()

    version = parse_version(info["version"]) or "0.1"

    allversions = [version]
    for file in files["files"]:
        tmp_ver = parse_version(file["version"])
        if tmp_ver:
            allversions.append(tmp_ver)

    print([file["version"] for file in files["files"]])
    print(allversions)

    # Mod author may not have updated the mod version.
    # Version used should be the newest file version among the files we selected
    version = get_max_version(allversions)

    print("Version is: " + version)
    atom = Atom(parse_name(info["name"]) + "-" + version)
    print(atom)

    # Select all files except those in the OLD_VERSION category
    tmpfiles = [
        file
        for file in files["files"]
        if file["category_name"] != "OLD_VERSION" and file["category_name"]
    ]
    files = []
    minorversion = None
    for file in tmpfiles:
        skip = False

        # Ignore exe files. We can't use them anyway
        _, ext = os.path.splitext(file["file_name"])
        if ext == ".exe":
            skip = True

        for otherfile in tmpfiles:
            # Base name is the same, but filename is different.
            # That is, they've uploaded a new version but didn't change the version
            # Let's change it for them to avoid conflicts
            if (
                file["name"] == otherfile["name"]
                and file["file_name"] != otherfile["file_name"]
            ):
                if minorversion is None:
                    minorversion = 1
                else:
                    minorversion += 1

                if file["file_name"] < otherfile["file_name"]:
                    skip = True
                    break

        if not skip:
            files.append(file)

    return NexusData(
        atom=Atom(atom + (str(minorversion or ""))),
        name=info["name"],
        desc=info["summary"],
        files=[file["file_name"].replace(" ", "_") for file in files],
        homepage=f"https://www.nexusmods.com/{game}/mods/{modid}",
        author=info["author"],
        nudity=info["contains_adult_content"],
    )


def validate_file(game, mod_id, file):
    hash = get_hash(get_filename(file), hashlib.md5)
    hash_url = f"https://api.nexusmods.com/v1/games/{game}/mods/md5_search/{hash}.json"

    headers = {"APIKEY": NEXUS_KEY, "content-type": "application/json"}

    response = requests.get(hash_url, headers=headers)

    if response.status_code == requests.codes.not_found:
        return False

    mods = json.loads(response.text)
    for mod in mods:
        if mod["mod"]["mod_id"] == mod_id:
            return True

    modnames = [mod.get("mod").get("name") for mod in mods]
    raise Exception(
        f"Invalid response {modnames} from NexusMods API when hashing {file}"
    )
